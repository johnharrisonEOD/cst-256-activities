@extends('layouts.appmaster')

@section('title')
    Order 3
@endsection

@section('content')

    @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
    @endif
    <form action = "doOrder3" method = "POST">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
        <h2>Order</h2>
        <table>
            <tr>
                <td>First Name</td>
                <td><input type = "text" name = "first_name" /></td>
            </tr>

            <tr>
                <td>Last Name</td>
                <td><input type = "text" name = "last_name" /></td>
            </tr>

            <tr>
                <td>Product</td>
                <td><input type = "text" name = "product" /></td>
            </tr>

            <tr>
                <td colspan = "2" align = "center">
                    <input type = "submit" value = "Place Order" />
                </td>
        </table>
    </form>
@endsection
