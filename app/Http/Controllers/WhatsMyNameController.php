<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WhatsMyNameController extends Controller
{
    //
    public function index(Request $request)
    {
        // Get the name
        $first_name = $request->input('firstname');
        $last_name = $request->input('lastname');
        echo "Your name is: ".$first_name." ".$last_name;
        echo "<br>";

        // Usage of path method
        $path = $request->path();
        echo "Path Method: ".$path;
        echo "<br>";

        // Usage of is method
        $method = $request->isMethod('get')?"GET":"POST";
        echo "GET or POST Method: ".$method;
        echo "<br>";

        // Usage of url method
        $url = $request->url();
        echo "URL methhod: ".$url;
        echo "<br>";

        $data = ['first_name' => $first_name, 'last_name' => $last_name];

        return view('thatswhoami')->with($data);
    }
}
