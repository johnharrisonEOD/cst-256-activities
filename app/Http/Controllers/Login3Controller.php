<?php


namespace App\Http\Controllers;

use App\Models\UserModel;
use App\Services\Business\SecurityService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class Login3Controller extends Controller
{

    public function index(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $request->validate
        ([
            'username' => 'Required | Between:4,10 | Alpha',
            'password' => 'Required | Between:4,10'
        ]);

        $userModel = new UserModel($username, $password);
        $service = new SecurityService();


        if($service->login($userModel))
        {
            $data = ['userModel' => $userModel];
            return view('login2Passed')->with($data);
        }
        return view('loginFailed');

    }
}

