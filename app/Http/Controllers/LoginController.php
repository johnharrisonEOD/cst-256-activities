<?php


namespace App\Http\Controllers;

use App\Models\UserModel;
use App\Services\Business\SecurityService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class LoginController extends Controller
{

    public function index(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $userModel = new UserModel($username, $password);
        $service = new SecurityService();


        if($service->login($userModel))
        {
            $data = ['userModel' => $userModel];
            return view('login2Passed')->with($data);
        }
        return view('loginFailed');

    }
}
