<?php


namespace App\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Services\Business\OrderService;

class OrderController extends Controller
{

    public function index(Request $request)
    {
        $firstName = $request->get('first_name');
        $lastName = $request->get('last_name');
        $product = $request->get('product');

        $order_service = new OrderService();

        $order_service->createOrder($firstName,$lastName,$product);

        return view('order');
    }

}
