<?php


namespace App\Services\Business;

use App\Services\Data\SecurityDAO;
class SecurityService
{

    public function login($userModel)
    {
        $dao = new SecurityDAO();
        return $dao->findByUser($userModel);
    }
}
