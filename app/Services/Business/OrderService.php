<?php


namespace App\Services\Business;
use App\Services\Data\OrderDAO;
use App\Services\Data\CustomerDAO;


class OrderService
{
    private function connectDB()
    {
        $conn = mysqli_connect('localhost','root','root','activity2','8889');
        return $conn;
    }

    public function createOrder($firstName, $lastName, $product)
    {
        $conn = $this->connectDB();
        $conn->autocommit(false);
        $conn->begin_transaction();

        $customerDAO = new CustomerDAO($conn);
        $orderDAO = new OrderDAO($conn);

        $sql = "SELECT id FROM activity2.customers WHERE first_name = '$firstName' AND last_name = '$lastName';";
        $id = $conn->query($sql)->fetch_assoc()['id'];


        $customerDAO->addCustomer($firstName,$lastName);
        $orderDAO->addOrder($product,$id);

        $conn->commit();
        echo $firstName." ".$lastName." ordered ".$product." with a customer id of ".$id;
    }

}
